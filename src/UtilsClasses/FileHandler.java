package UtilsClasses;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class FileHandler {
    /**
     * buffered file reader reads file in lines
     * @param sourcefile arrayist that will hold the data
     * @param path src file
     * @param delim delimeter how to split the lines (" ", ";")
     */
    public static void bufferedReadFile(List<List<Integer>> sourcefile, String path, String delim){
        try (BufferedReader br = new BufferedReader(new FileReader(path, StandardCharsets.UTF_8))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(delim);
                Integer[] valuesInteger = new Integer[values.length];
                int i = 0;
                for (String s:values) valuesInteger[i++] = Integer.parseInt(s);
                sourcefile.add(Arrays.asList(valuesInteger));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static ArrayList<Integer> readFile(String path){
        ArrayList<Integer> output = new ArrayList<>();
        File src = new File(path);
        try (Scanner sc = new Scanner(src)) {
            while (sc.hasNextInt()) {
                output.add(sc.nextInt());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found: "+e);
        }
        return output;
    }

    public static ArrayList<String> readFileAsString(String path){
        ArrayList<String> output = new ArrayList<>();
        File src = new File(path);
        try (Scanner sc = new Scanner(src)) {
            while (sc.hasNext()) {
                output.add(sc.next());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found: "+e);
        }
        return output;
    }

    public static void writeFile2dim(String path, ArrayList<ArrayList<Object>> src, int first) throws IOException {
        FileWriter writer = new FileWriter(path);
        for (ArrayList<Object> array: src) {
            for (int i = first; i < array.size(); i++) {
                Object x = array.get(i);
                if ("java.lang.String".equals(x.getClass().getName())) {
                    writer.write((String) x);
                } else {
                    if (i==0) writer.write(""+x);
                    else writer.write(" "+x);
                }
            }
            writer.write("\n");
        }
        writer.close();
    }

    public static void writeFileAsString(String path, String src) throws IOException {
        FileWriter writer = new FileWriter(path);
        writer.write( src);
        writer.write("\n");
        writer.close();
    }

    public static void writeFile(String path, ArrayList<Object> src, int first) throws IOException {
        FileWriter writer = new FileWriter(path);

        for (int i = first; i < src.size(); i++) {
            Object x = src.get(i);
            if ("java.lang.String".equals(x.getClass().getName())) {
                writer.write((String) x);
            } else {
                if (i==0) writer.write(""+x);
                else writer.write(" "+x);
            }
        }
        writer.write("\n");
        writer.close();
    }
}
