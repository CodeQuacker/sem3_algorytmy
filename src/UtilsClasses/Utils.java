package UtilsClasses;

import java.util.ArrayList;

public class Utils {

    /**
     * Sortowanie countsort wykorzystywane do optymalnej wersji algorytmu zadania 2
     * @param in tablica wejściowa
     * @param first pierwsza wartość która należy posortować
     * @param limit maksymalna wartość wprowadzonych wartości
     * @return posortowana 1-wym. tablica
     */
    public static ArrayList<Integer> countSort(ArrayList<Integer> in, int first, int limit){
        ArrayList<Integer> csort = new ArrayList<>(), out = new ArrayList<>();
        int operations = 0;

        for (int i=1; i<=limit; i++){
            csort.add(0);
        }
        for (int i=0; i<first; i++) out.add(in.get(i));
        for (int i=first; i<in.size(); i++) csort.set(in.get(i), csort.get(in.get(i))+1);

        for (int i=0; i<csort.size(); i++){
            for (int j=0; j<csort.get(i); j++){
                out.add(i);
                operations++;
            }
        }
        out.add(0, operations);
        return out;
    }

    //It's just bubble sort!
    /**
     * Sortowanie bąbelkowe wykorzystywane do nieoptymalnej wersji algorytmu zadania 2
     * @param array tablica wejściowa
     * @param inSettings ilość wartości które zawierają informacje o tablicy
     * @return posortowana 1-wym. tablica
     */
    public static ArrayList<Integer> totallyNotOptimalSort(ArrayList<Integer> array, int inSettings) {
        int operations = 0;
        for (int i=inSettings; i<array.size()-1; i++){
            int a = array.get(i), b=array.get(i+1);
            if (a > b){
                array.set(i, b);
                array.set(i+1, a);
                i=inSettings-1;
                operations++;
            }
        }
        array.add(0, operations);
        return array;
    }

    /**
     * Funkcja przygotowuje tablicę dla z01 problemu 2. Zawiera ustawianie par liczb w najmniejszą ilość par
     * @param in tablica wejściowa
     * @param argLines ilość wartości w tablicy
     * @param argLimit maksymalna wartość sumy pary
     * @param inSettings ilość wartości które zawierają informacje o tablicy
     * @return 2-wym. tablica z ustawionymi parami wartościami oraz ilością par
     */
    public static ArrayList<ArrayList<Object>> prepareOutArray(ArrayList<Integer> in, int argLines, int argLimit, int inSettings) {
        ArrayList<ArrayList<Object>> out = new ArrayList<>();
        int first= inSettings, last= argLines + inSettings -1, outNr=0;

        while(first<=last){
            out.add(outNr, new ArrayList<>());
            if (first == last) {
                out.get(outNr).add(in.get(first));
                break;
            }
            if (in.get(first)+ in.get(last) <= argLimit){
                out.get(outNr).add(in.get(first));
                out.get(outNr).add(in.get(last));
                first++;
                if (first != last) last--;
            }else if (in.get(first)+ in.get(last) > argLimit){
                out.get(outNr).add(in.get(last));
                last--;
            }
            outNr++;
        }
        out.add(new ArrayList<>());
        out.get(outNr+1).add(outNr+1);
        return out;
    }
}

