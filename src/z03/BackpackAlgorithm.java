package z03;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static UtilsClasses.FileHandler.*;

public class BackpackAlgorithm {
    private static int[][] V;
    private static int[] w, p;
    private static int timesMaxP = 0, maxP = 0;

    public static void main(String[] args) throws IOException {
        ArrayList<Integer> in = readFile("src/z03/InOutFiles/in0302.txt");

        int n = in.remove(0), maxW = in.remove(0), i = 0;
        ArrayList<ArrayList<Object>> pickedItems = new ArrayList<>();
        p = new int[n];
        w = new int[n];

        while (!in.isEmpty()) {
            p[i] = in.remove(0);
            w[i++] = in.remove(0);
        }

//        System.out.println("\n" +
//        "[0, 0, 0, 0, 0]\n" +
//        "[0, 2, 2, 2, 2]\n" +
//        "[0, 2, 3, 3, 3]\n" +
//        "[0, 2, 5, 5, 5]\n" +
//        "[0, 2, 5, 5, 5]\n" +
//        "[0, 2, 5, 5, 5]\n" +
//        "[0, 2, 5, 6, 6]\n");

        int[][] knapsack = new int[maxW + 1][n + 1];
        KnapsackInitialize(knapsack, p, w, maxW, n);

        for (int[] k : knapsack) {
            System.out.println(Arrays.toString(k));
        }
        System.out.println();

        final int maxP = knapsack[maxW][n];
        KnapsackPickItems(knapsack, pickedItems, p, w, maxW, n, maxP);

        for (int[] k : knapsack) {
            System.out.println(Arrays.toString(k));
        }

        System.out.println(pickedItems.toString());

        writeFile2dim("src/z03/InOutFiles/out0302.txt", pickedItems, 0);


    }

    private static void KnapsackPickItems(int[][] knapsack, List<ArrayList<Object>> pickedItems, int[] p, int[] w, int maxW, int n, int maxP) {
        int i = 0;
        for (int y = maxW; y >= 0; y--) {
            for (int x = n; x >= 0; x--) {
                if (knapsack[y][x] == maxP) {
                    pickedItems.add(new ArrayList<>());
                    PickItems(knapsack, pickedItems.get(i++), p, w, y, x);
                } else return;
            }
        }
    }

    private static void PickItems(int[][] knapsack, List<Object> items, int[] p, int[] w, int y, int x) {
        int points = knapsack[y][x], currX = x, currY = y;
        while (knapsack[y][x] != points) y--;
        knapsack[currY][currX] = 0;
        points -= p[x - 1];
        items.add(x--);

        while (points > 0) {
            while (knapsack[y][x] != points && knapsack[y][x] != 0) y--;
            if (knapsack[y][x] == 0) {
                items.clear();
                break;
            }
            while (knapsack[y][x] == knapsack[y - 1][x] && knapsack[y][x] != 0) y--;
            while (knapsack[y][x] == points && knapsack[y][x] == knapsack[y][x - 1]) x--;
            points -= p[x - 1];
            items.add(x--);
        }
    }

    private static void KnapsackInitialize(int[][] knapsack, int[] p, int[] w, int maxW, int n) {
        for (int y = 0; y <= maxW; y++) {
            for (int x = 0; x <= n; x++) {
                if (x == 0 || y == 0) knapsack[y][x] = 0;
                else if (w[x - 1] > y) knapsack[y][x] = knapsack[y][x - 1];
                else if (w[x - 1] == y) knapsack[y][x] = Math.max(p[x - 1], knapsack[y][x - 1]);
                else if (w[x - 1] < y)
                    knapsack[y][x] = Math.max(knapsack[y - w[x - 1]][x - 1] + p[x - 1], knapsack[y][x - 1]);
            }
        }
    }
}
