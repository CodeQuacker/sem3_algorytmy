package z03;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static UtilsClasses.FileHandler.bufferedReadFile;
import static UtilsClasses.FileHandler.writeFile;
import static java.lang.System.arraycopy;

public class KruskalAlgorithm {
    public static void main(String[] args) throws IOException {
        List<List<Integer>> in = new ArrayList<>();
        int[] subset;
        List<Node> inNodes = new ArrayList<>();
        List<Node> kruskal = new ArrayList<>();

        bufferedReadFile(in, "src/z03/InOutFiles/in0303.txt", " ");
//        writeFile2dim("src/z03/InOutFiles/in0303.txt", );

        int n = in.remove(0).get(0);
        subset = new int[n];
        for (int i = 0; i < n; i++) {
            subset[i] = i+1;
        }

        for (int i = 0; i < n; i++) {
            List<Integer> buffer = new ArrayList<>(in.remove(0));
            while (!buffer.isEmpty()) {
                Integer x1 = buffer.remove(0);
                Integer x2 = buffer.remove(0);
                inNodes.add(new Node(i + 1, x1, x2));
            }
        }

        int nodeTranslation = 0;
        Node[] inByValue = new Node[inNodes.size()];
        for (Node x : inNodes) inByValue[nodeTranslation++] = x;

        mergeSort(inByValue);

        System.out.println();
        for (Node x : inByValue) System.out.print(x + " ");

        int edgeCount = 0, nextNode = 0;
        while (edgeCount < n-1){
            Node current = inByValue[nextNode++];
            int x = current.start, y = current.end;
            int dstSubset = subset[y - 1], srcSubset = subset[x - 1];
            if (srcSubset != dstSubset){
                for (int i = 0; i < n; i++) {
                    if (subset[i] == dstSubset) {
                        subset[i] = srcSubset;
                    }
                }
                kruskal.add(current);
                edgeCount++;
            }
            System.out.println();
            for (int i : subset) System.out.print(i + " ");
            System.out.println();
            System.out.println(kruskal);
        }

        System.out.println();
        System.out.println(kruskal);
        for (Integer i:subset) System.out.print(i + " ");

        ArrayList<Object> out = new ArrayList<>();
        int sumValue = 0;
        for (Node sumn : kruskal) sumValue += sumn.value;
        out.add(kruskal + "\n");
        out.add(sumValue);

        writeFile("src/z03/InOutFiles/out0303.txt", out, 0);

    }

    static void mergeSort(Node[] in){
        int n = in.length;
        if (n<2) return;
        int mid = n/2;

        Node[] l = new Node[mid], r = new Node[n-mid];

        arraycopy(in, 0, l, 0, mid);
        arraycopy(in, mid, r, 0, n - mid);

        mergeSort(l);
        mergeSort(r);

        merge(in, l, r);

    }

    static void merge(Node[] in, Node[] l, Node[] r){
        int i = 0, j = 0, k = 0, left = l.length, right = r.length;

        while (i < left && j < right){
            if (l[i].value <= r[j].value) in[k++] = l[i++];
            else in[k++] = r[j++];
        }
        while (i < left) in[k++] = l[i++];
        while (j < right) in[k++] = r[j++];
    }

}

class Node {
    int start, end, value;

    Node(int start, int end, int value) {
        this.start = start;
        this.end = end;
        this.value = value;
    }

    @Override
    public String toString() {
        return "<" + start + "," + end + ">(" + value + ')';
    }
}
