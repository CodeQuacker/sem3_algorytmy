package z01;

import java.io.IOException;
import java.util.ArrayList;

import static UtilsClasses.FileHandler.*;
import static UtilsClasses.Utils.countSort;
import static UtilsClasses.Utils.prepareOutArray;

public class Problem2 {
    public static void main(String[] args) throws IOException {
        ArrayList<Integer> in = readFile("src/z01/InOutFiles/in0102.txt");
        //noinspection UnusedAssignment
        ArrayList<ArrayList<Object>> out = new ArrayList<>();
        int argLines=in.get(0), argLimit= in.get(1),inSettings=2;

        in = countSort(in,inSettings,argLimit);
        System.out.println("Operations: "+in.remove(0));

        out = prepareOutArray(in, argLines, argLimit, inSettings);
        writeFile2dim("src/z01/InOutFiles/out0102.txt", out ,0);
    }
}