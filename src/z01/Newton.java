package z01;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import static UtilsClasses.FileHandler.readFile;
import static UtilsClasses.FileHandler.writeFile2dim;
import static java.lang.Math.max;
import static java.lang.Math.min;

public class Newton {
    public static int operationsN2 = 0;
    public static int operationsN5 = 0;

    public static void main(String[] args) throws IOException {
        ArrayList<Integer> in;
        ArrayList<ArrayList<Object>> out = new ArrayList<>();
        in = readFile("src/z01/InOutFiles/in0101.txt");

        int newt2 = newton02(in.get(0),in.get(1));
        int newt5 = newton05(in.get(0),in.get(1));

        out.add(new ArrayList<>());
        out.add(new ArrayList<>());
        Collections.addAll(out.get(0), "SN2 = ", newt2, ", Licz =", operationsN2);
        Collections.addAll(out.get(1), "SN5 = ", newt5, ", Licz =", operationsN5);

        writeFile2dim("src/z01/InOutFiles/out0101.txt", out, 0);

    }

    /**
     * Funkcja wykonująca wersję II zad. 1
     * Wylicza symbol newtona z definicji po uproszczeniu przez większy czynnik mianownika
     * @param n górna liczba symboluu newtona
     * @param k dolna liczba symboluu newtona
     * @return int - wyliczona wartość symbolu newtona
     */
    private static int newton02(int n, int k){
        if (k>n) return n;
        if (k==0 || k==n) return n;
        int p = max(k, (n - k));
        int r = min(k, (n - k));
        return strong(n)/strong(p)/strong(r);
    }

    /**
     * Funkcja wykonująca wersję II zad. 1
     * Wylicza symbol newtona z trójkąta pascala w wersji tablicy 2-wymiarowej
     * @param n górna liczba symboluu newtona
     * @param k dolna liczba symboluu newtona
     * @return int - wyliczona wartość symbolu newtona
     */
    private static int newton05(int n, int k) {
        ArrayList<ArrayList<Integer>> pascal = createPascal(n);
        return pascal.get(n).get(k);
    }

    /**
     * Funkcja tworzy 2-wymiarową tablicę Pascala
     * @param n ilość poziomów
     * @return 2-wym. tablica Pascala
     */
    private static ArrayList<ArrayList<Integer>> createPascal(int n) {
        ArrayList<ArrayList<Integer>> pascal = new ArrayList<>();
        pascal.add(new ArrayList<>());
        pascal.add(new ArrayList<>());
        pascal.get(0).add(1);
        pascal.get(1).add(1);
        pascal.get(1).add(1);
        for (int i = 2; i<= n; i++){
            pascal.add(new ArrayList<>());
            for (int j=0; j<=i; j++){
                if (j==0 || j==i) pascal.get(i).add(1);
                else {
                    pascal.get(i).add(pascal.get(i - 1).get(j - 1) + pascal.get(i - 1).get(j));
                    operationsN5++;
                }
            }
        }
        return pascal;
    }

    /**
     * funkcja wylicza silnię podanej liczby
     * @param in podstawa silni
     * @return int - wartość silni podanej liczby
     */
    private static int strong(int in){
        int out = 1;
        for (int i=2; i<=in; i++) {
            out *= i;
            operationsN2++;
        }
        return out;
    }
}


