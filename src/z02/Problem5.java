package z02;

import java.io.IOException;
import java.util.ArrayList;

import static UtilsClasses.FileHandler.readFile;
import static UtilsClasses.FileHandler.writeFileAsString;

public class Problem5 {

    public static void main(String[] args) throws IOException {
        ArrayList<Integer> in = readFile("src/z02/InOutFiles/in0205.txt");
        System.out.println(in);
        int a = in.remove(0), b = in.remove(0);
        int nwd;
        System.out.println(nwd = NWD(a,b));
        writeFileAsString("src/z02/InOutFiles/out0205.txt", "NWD(" +a+ "," +b+ ")=" + nwd);
    }

    /**
     * Rekurencyjna funkcja zwracająca NWD
     * @param a pierwsza liczba
     * @param b druga liczba
     * @return int
     */
    private static int NWD(int a, int b) {
        if (b==0) return a;
        else return NWD(b, a%b);
    }
}
