package z02;

import java.io.IOException;
import java.util.ArrayList;

import static UtilsClasses.FileHandler.*;
import static z02.DIRECTION.*;

public class Problem6 {
    public static void main(String[] args) throws IOException {
        ArrayList<String> array = readFileAsString("src/z02/InOutFiles/in0206.txt");
        int n = Integer.parseInt(array.remove(0));
        char[] word = array.remove(0).toCharArray();

        char[][] msq = new char[n][n];
        magicsquare(msq, 0,n,word);

        ArrayList<Object> out = new ArrayList<>();
        for (char[] w : msq){
            StringBuilder str = new StringBuilder();
            for (char c : w) str.append(c);
            str.append("\n");
            out.add(str.toString());
        }
        writeFile("src/z02/InOutFiles/out0206.txt", out,0);
    }

    /**
     * Rekurencyjna funkcja twożąca magiczny kwadrat
     * @param array tablica magicznego trójkąta
     * @param min dolna granica indeksu do wpisywania słowa do magicznego kwadratu
     * @param max górna granica indeksu ^
     * @param word tablica char[] słowa do wypisania
     */
    public static void magicsquare(char[][] array, int min, int max, char[] word){
        int x = min, y = min, index = 0;
        boolean end = false;
        DIRECTION dir = RIGHT;
        while (!end){
            if (index == word.length) index = 0;
            array[x][y] = word[index++];
            switch (dir){
                case RIGHT -> {
                    y++;
                    if(y+1 == max) dir = DOWN;
                    if (y==max) end = true; //dla nieparzystej wielkości kończy po wpisaniu jednego chara
                }
                case DOWN -> {
                    x++;
                    if(x+1 == max) dir = LEFT;
                }
                case LEFT -> {
                    y--;
                    if(y == min) dir = UP;
                }
                case UP -> {
                    x--;
                    if(x == min) end = true;
                }
            }
        }
        if (min+1<max-1)magicsquare(array, min+1, max-1, word);
    }



}
