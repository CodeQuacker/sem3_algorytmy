package z02;

import java.io.IOException;
import java.util.ArrayList;

import static UtilsClasses.FileHandler.readFile;
import static UtilsClasses.FileHandler.writeFile;
import static java.lang.System.arraycopy;

public class Problem1 {
    static int o = 0;


    public static void main(String[] args) throws IOException {
        ArrayList<Object> out = new ArrayList<>();
        ArrayList<Integer> in = readFile("src/z02/InOutFiles/in0201.txt");
        int n = in.remove(0);
        int[] array = new int[n];

        for (int i=0; i<n; i++){
            array[i] = in.remove(0);
        }
        mergeSort(array, n);
        for (int x : array) out.add(x);
        writeFile("src/z02/InOutFiles/out0201.txt", out, 0);
    }

    /**
     * Rekurencyjny algorytm merge sort
     * @param in wejściowa tablica intp[]
     * @param n ilość elementów tablicy
     */
    private static void mergeSort(int[] in, int n){
        if (n<2) return;
        int mid = n/2;
        int[] l = new int[mid], r = new int[n-mid];
        arraycopy(in, 0, l, 0, mid);
        arraycopy(in, mid, r, 0, n - mid);

        mergeSort(l, mid);
        mergeSort(r, n-mid);

        merge(in, l, r, mid, n-mid);

    }

    /**
     * Część algorytmu merge sort, funkcja scalająca
     * @param in tablica wejściowa int[]
     * @param l tablica części lewej do scalenia int[]
     * @param r tablica części prawej do scalenia int[]
     * @param left ilość elementów tablicy lewej
     * @param right ilość elementów tablicy prawej
     */
    private static void merge(int[] in, int[] l, int[] r, int left, int right){
        int i = 0, j = 0, k = 0;

        while (i < left && j < right){
            if (l[i] <= r[j]) in[k++] = l[i++];
            else in[k++] = r[j++];
        }
        while (i < left) in[k++] = l[i++];
        while (j < right) in[k++] = r[j++];
    }
}